$('.carousel').carousel({
    interval: 1500
})

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

$('#modelId').on('show.bs.modal', function(e){
    console.log('Se abre el modal')
    $('.info').removeClass('btn')
    $('.info').addClass('btn disabled btn-rojo')
})

$('#modelId').on('shown.bs.modal', function(e){
    console.log('El modal esta abierto')
})

$('#modelId').on('hide.bs.modal', function(e){
    console.log('Se cierra el modal')
})

$('#modelId').on('hidden.bs.modal', function(e){
    console.log('El modal se cerro')
    $('.info').removeClass('disabled btn-rojo')
    $('.info').addClass('btn')
})