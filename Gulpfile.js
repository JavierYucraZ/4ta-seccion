const gulp = require('gulp')
const sass = require('gulp-sass')
const browserSync = require('browser-sync')
const del = require('del')
const imagemin = require('gulp-imagemin')
const uglify = require('gulp-uglify')
const usemin = require('gulp-usemin')
const rev = require('gulp-rev')
const cleancss = require('gulp-clean-css')
const flatmap = require('gulp-flatmap')
const htmlmin = require('gulp-htmlmin')

gulp.task('sass', () => {
    return gulp.src('./css/*.scss').pipe(sass().on('err', sass.logError))
        .pipe(gulp.dest('./css'))
})

gulp.task('sass:watch', () => {
    return gulp.watch('./css/*.scss', gulp.series('sass'))
})

gulp.task('copyfonts', () => {
    return gulp.src('./node_modules/font-awesome/fonts/*.{otf,eot,svg,ttf,woff,woff2}').pipe(gulp.dest('./dist/fonts'))
})

gulp.task('browser-sync', () => {
    const files = ['./*.html', './css/*.css', './assets/*.{png, jpg, jpeg, gif}', './js/*.js']
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    })
})

gulp.task('clean', () => {
    return del(['dist'])
})

gulp.task('imagemin', () => {
    return gulp.src('./assets/*.{jpg,jpeg,gif,png}').pipe(imagemin({
        optimizationLevel: 3,
        progressive: true,
        interlaced: true
    })).pipe(gulp.dest('dist/assets'))
})

gulp.task('usemin', () => {
    return gulp.src('*.html').pipe(flatmap(function (stream, file) {
            return stream.pipe(usemin({
                    css: [rev()],
                    html: [function () {
                        return htmlmin({
                            collapseWhitespace: true
                        })
                    }],
                    js: [uglify(), rev()],
                    inlinejs: [uglify()],
                    inlinecss: [cleancss(), 'concat']
                }));
        })).pipe(gulp.dest('dist/'));

});

gulp.task('build', gulp.series('clean', 'copyfonts', 'imagemin', 'usemin'))

gulp.task('default', gulp.parallel('browser-sync', 'sass:watch'))